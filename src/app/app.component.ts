import {Component, effect, signal} from '@angular/core';
import {BehaviorSubject, map, Observable} from "rxjs";
import {} from "rxjs/operators";
import {Store} from "@ngrx/store";
import {selectBaseUri} from "./shared/config/store/config.selector";
import {ConfigAction} from "./shared/config/store/config.action";
import {PostService} from "./services/post.service";
import {IUser} from "./shared/security/models/user.model";
import {selectAccessToken, selectUser} from "./shared/security/store/security.selector";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  private posts$ = signal<any[]>([])
  private obsPosts$ = new BehaviorSubject<any[]>([])

  protected count: number = 0
  private countRef = effect(() => {
    this.count = this.posts$().length
  })

  protected count$ = this.obsPosts$.pipe(
    map(t => t.length)
  )
  get Posts$() { return this.posts$.asReadonly() }
  get ObsPosts$() { return this.obsPosts$.asObservable() }

  ngOnInit() {
    this.posts$.set([{ id: 1 }])
    this.obsPosts$.next([{ id: 1 }])
  }

  clickHandler() {
    this.posts$.update((data) => [...data, { id: 2 }])
    this.obsPosts$.next([...this.obsPosts$.value, { id: 2 }])
  }
}
