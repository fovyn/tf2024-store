import {APP_INITIALIZER, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {Store, StoreModule} from "@ngrx/store";
import {StoreDevtools, StoreDevtoolsModule} from "@ngrx/store-devtools";
import {ConfigModule} from "./shared/config/config.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ConfigState} from "./shared/config/store/config.state";
import {environment} from "../environments/environment";
import {ConfigAction} from "./shared/config/store/config.action";
import {SecurityModule} from "./shared/security/security.module";

const configInit = function ($store: Store, $http: HttpClient) {
  return () => {
    $http.get<ConfigState>(`/assets/config/${environment.mode}.json`).subscribe(config => $store.dispatch(ConfigAction.load({config})))
  }
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(),
    StoreDevtoolsModule.instrument(),
    ConfigModule,
    SecurityModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: ($store: Store, $http: HttpClient) => configInit($store, $http),
      deps: [Store, HttpClient],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
