export interface ConfigState {
  base_uri: string
}

export const initialState: ConfigState = {
  base_uri: ''
}

export const FEATURE_CONFIG = "config"
