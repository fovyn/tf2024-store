import {createActionGroup, props} from "@ngrx/store";
import {ConfigState, FEATURE_CONFIG} from "./config.state";

export const ConfigAction = createActionGroup({
  source: FEATURE_CONFIG,
  events: {
    load: props<{ config: ConfigState }>()
  }
})
