import {createFeatureSelector, createSelector} from "@ngrx/store";
import {ConfigState, FEATURE_CONFIG} from "./config.state";

const feature = createFeatureSelector<ConfigState>(FEATURE_CONFIG)

export const selectConfig = createSelector(feature, s => s)
export const selectBaseUri = createSelector(feature, s => s.base_uri)
