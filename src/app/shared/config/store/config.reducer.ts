import {createReducer, on} from "@ngrx/store";
import {initialState} from "./config.state";
import {ConfigAction} from "./config.action";

export const configReducer = createReducer(
  initialState,
  on(ConfigAction.load, (oldState, {config}) => ({...config}))
)
