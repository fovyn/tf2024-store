import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StoreModule} from "@ngrx/store";
import {FEATURE_CONFIG} from "./store/config.state";
import {config} from "rxjs";
import {configReducer} from "./store/config.reducer";



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(FEATURE_CONFIG, configReducer)
  ]
})
export class ConfigModule { }
