import {createFeatureSelector, createSelector} from "@ngrx/store";
import {FEATURE_SECURITY, SecurityState} from "./security.state";

const feature = createFeatureSelector<SecurityState>(FEATURE_SECURITY)

export const selectAccessToken = createSelector(feature, s => s.accessToken)
export const selectUser = createSelector(feature, s => s.user)
export const selectUserRole = createSelector(selectUser, (u) => u.roles)
