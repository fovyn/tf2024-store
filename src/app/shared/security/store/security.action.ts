import {createActionGroup, emptyProps, props} from "@ngrx/store";
import {SecurityState, FEATURE_SECURITY} from "./security.state";
import {IUser} from "../models/user.model";

export const SecurityAction = createActionGroup({
  source: FEATURE_SECURITY,
  events: {
    signIn: props<{ accessToken: string, user: IUser }>(),
    signOut: emptyProps()
  }
})
