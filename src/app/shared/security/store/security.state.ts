import {IUser} from "../models/user.model";

export interface SecurityState {
  accessToken: string,
  user: IUser
}

export const initialState: SecurityState = {
  accessToken: '',
  user: { id: 0, email: '', roles: '' }
}

export const FEATURE_SECURITY = "security"
