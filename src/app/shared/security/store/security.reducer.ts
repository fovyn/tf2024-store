import {createReducer, on} from "@ngrx/store";
import {initialState} from "./security.state";
import {SecurityAction} from "./security.action";

export const securityReducer = createReducer(
  initialState,
  on(SecurityAction.signIn, (oldState, { accessToken, user}) => {
    console.log("REDUCER")
    return ({accessToken, user})
  }),
  on(SecurityAction.signOut, (oldState) => ({...initialState}))
)
