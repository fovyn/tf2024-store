import {FormControl, FormGroup, Validators} from "@angular/forms";

export interface IUser {
  id: number,
  email: string,
  roles: string
}

export const fUserSignIn = function () {
  return new FormGroup({
    email: new FormControl(null, [Validators.email, Validators.required]),
    password: new FormControl(null, [Validators.required])
  })
}
