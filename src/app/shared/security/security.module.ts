import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInComponent } from './components/sign-in/sign-in.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthService} from "./services/auth.service";
import {StoreModule} from "@ngrx/store";
import {FEATURE_SECURITY} from "./store/security.state";
import {securityReducer} from "./store/security.reducer";



@NgModule({
  declarations: [
    SignInComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature(FEATURE_SECURITY, securityReducer)
  ],
  providers: [
    AuthService,
  ],
  exports: [
    SignInComponent,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SecurityModule { }
