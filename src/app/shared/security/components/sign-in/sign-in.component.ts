import { Component } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {fUserSignIn} from "../../models/user.model";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'security-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrl: './sign-in.component.scss'
})
export class SignInComponent {
  form: FormGroup = fUserSignIn()

  constructor(
    private $auth: AuthService
  ) {
  }

  submitHandler() {
    if (this.form.valid) {
      const { email, password } = this.form.value
      this.$auth.signIn(email, password).subscribe(data => console.log(data))
    }
  }
}
