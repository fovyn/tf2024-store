import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Store} from "@ngrx/store";
import {selectBaseUri} from "../../config/store/config.selector";
import {switchMap, tap} from "rxjs";
import {SecurityState} from "../store/security.state";
import {SecurityAction} from "../store/security.action";

@Injectable()
export class AuthService {

  constructor(
    private $http: HttpClient,
    private $store: Store
  ) { }

  signIn(email: string, password: string) {
    return this.$store.select(selectBaseUri).pipe(
      switchMap(uri => this.$http.post<SecurityState>(`${uri}/signIn`, { email, password })),
      tap(data => this.$store.dispatch(SecurityAction.signIn(data)))
    )
  }
}
