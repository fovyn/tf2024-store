import {Injectable, signal} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Store} from "@ngrx/store";
import {selectBaseUri} from "../shared/config/store/config.selector";
import {switchMap, tap} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PostService {
  data$ = signal<any[]>([])

  constructor(
    private readonly $http: HttpClient,
    private readonly $store: Store
  ) { }

  getAll() {
    return this.$store.select(selectBaseUri).pipe(
      switchMap(uri => this.$http.get<any[]>(`${uri}/posts`)),
      tap(data => console.log(data))
    ).subscribe(data => this.data$.set([...data]));
  }

  add() {
    this.data$.update((v) => [...v, { id: 2}])
  }
}
